from django.urls import path
from . import views

urlpatterns = [
    path("",views.home),
    path('registrarequipo/', views.registrarequipo),
    path('registrarestadio/', views.registrarestadio),
    path('registrarOwner/', views.registrarOwner),
    path('registrarjugador/', views.registrarjugador),
    
]