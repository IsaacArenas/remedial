from django.contrib import admin
from .models import Team, Player, Stadium, Owner

admin.site.register(Team)
admin.site.register(Player)
admin.site.register(Stadium)
admin.site.register(Owner)
