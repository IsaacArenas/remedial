from django.shortcuts import render, redirect
from home import models
# Create your views here.

def index(request):
    players = models.Player.objects.all()
    return render(request,"home/index.html",{'Players': players})


def estadio(request):
    estadio = models.Stadium.objects.all()
    return render(request,"home/Stadium.html",{'Stadium': estadio})

def equipos(request):
    equipos = models.Team.objects.all()
    return render(request,"home/Team.html",{'Team': equipos})

def Owner(request):
    Owner = models.Owner.objects.all()
    return render(request,"home/Owner.html",{'Owner': Owner})


# urls de guardar

def registrarjugador(request):
    name = request.POST['txtNombre']
    number = request.POST['txtNumero']
    position = request.POST['txtPosition']

    registrar = models.Player.objects.create(name=name, number=number, position=position)
    return redirect('/index/')


def registrarequipo(request):
    name = request.POST['txtNombre']

    registro = models.Team.objects.create(name=name)
    return redirect('/equipos/')


def registrarestadio(request):
    name = request.POST['txtNombre']
    capacity = request.POST['txtcapacidad']
    owner = request.user  

    registrar = models.Stadium.objects.create(name=name, capacity=capacity, owner=owner)
    return redirect('/estadio/')


def registrarOwner(request):
    name = request.POST['txtNombre']

    registro = models.Owner.objects.create(name=name)
    return redirect('/Owner/')

def eliminarequipo(request,name):
    name = request.objects.get(name = name)
    name.delete()
    return redirect('/equipos/')