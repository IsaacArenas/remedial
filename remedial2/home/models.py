from django.db import models

class Team(models.Model):
    name = models.CharField(max_length=50)

class Player(models.Model):
    name = models.CharField(max_length=50)
    number = models.IntegerField()
    position = models.CharField(max_length=50)
    team = models.ForeignKey(Team, on_delete=models.CASCADE)


class Stadium(models.Model):
    name = models.CharField(max_length=50)
    capacity = models.IntegerField()
    owner = models.ForeignKey('Owner', on_delete=models.CASCADE)
    

class Owner(models.Model):
    name = models.CharField(max_length=50)