"""
URL configuration for nfl2 project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/5.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from home import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.index, name = 'index'),
    path('estadio/', views.estadio, name = 'estadio'),
    path('equipos/', views.equipos, name = 'equipos'),
    path('Owner/', views.Owner, name = 'Owner'),
    path('registrarequipo/', views.registrarequipo, name = ''),
    path('registrarestadio/', views.registrarestadio, name = ''),
    path('registrarOwner/', views.registrarOwner, name = ''),
    path('registrarjugador/', views.registrarjugador, name = ''),
    path('eliminarequipo/<name>', views.eliminarequipo)
    
]
